package pl.edu.agh.traffic.assistant.mobile.agents;


import android.content.SharedPreferences;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import pl.edu.agh.ta.common.map.Route;
import pl.edu.agh.ta.common.map.StreetInfo;
import pl.edu.agh.ta.common.map.StreetState;
import pl.edu.agh.ta.common.jade.AbstractMessageFactory;
import pl.edu.agh.ta.common.util.Const;

/**
 * Created by Adiki on 30.04.14.
 */
public class StreetInfoRequestFactory extends AbstractMessageFactory {

    private final SharedPreferences sharedPreferences;

    public StreetInfoRequestFactory(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public ACLMessage createRouteRequest(int jctFrom, int jctTo) {
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setProtocol(Const.PROTOCOL_ROUTE_REQUEST);
        request.setContent(jctTo + "");
        AID r = new AID(Const.JUNCTION_AGENT_PREFIX + jctFrom + "@" + sharedPreferences.getString("platform_id", "?"), AID.ISGUID);
        r.addAddresses("http://" + sharedPreferences.getString("server_ip", "?") + ":7778/acc");
        request.addReceiver(r);
        request.setReplyWith(Const.PROTOCOL_ROUTE_REQUEST + System.currentTimeMillis());
        return request;
    }

    // Not needed in mobile agent
    @Override
    public ACLMessage createRouteResponse(ACLMessage request, Route route) {
        return null;
    }

    @Override
    public ACLMessage createStreetCostRequest(int jctFrom, int jctTo) {
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setProtocol(Const.PROTOCOL_STREET_COST_REQUEST);
        request.setContent(jctFrom + ";" + jctTo);
        AID r = new AID(Const.JUNCTION_AGENT_PREFIX + jctFrom + "@" + sharedPreferences.getString("platform_id", "?"), AID.ISGUID);
        r.addAddresses("http://" + sharedPreferences.getString("server_ip", "?") + ":7778/acc");
        request.addReceiver(r);
        request.setReplyWith(Const.PROTOCOL_STREET_COST_REQUEST + System.currentTimeMillis());
        return request;
    }

    // Not needed in mobile agent
    @Override
    public ACLMessage createStreetCostResponse(ACLMessage message, int cost) {
        return null;
    }
}
