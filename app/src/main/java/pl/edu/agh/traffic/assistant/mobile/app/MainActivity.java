package pl.edu.agh.traffic.assistant.mobile.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.EditText;

import java.lang.reflect.Field;
import java.util.logging.Level;

import jade.util.Logger;
import pl.edu.agh.traffic.assistant.mobile.agents.MobileAgentInstance;


public class MainActivity extends ActionBarActivity {
    private Logger logger = Logger.getJADELogger(this.getClass().getName());
    private EditText logTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logTextView = (EditText) findViewById(R.id.logTextView);

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        IntentFilter msgFilter = new IntentFilter();
        msgFilter.addAction("APPEND_MESSAGE");
        registerReceiver(new MyReceiver(), msgFilter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_start) {
            // TODO: Tutaj startujemy agenta. Przed odpaleniem apki musi być włączone gui JADE.
            // Trzeba ustawić IP i port zgodne z tym co pokazuje gui JADE.
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            new MobileAgentInstance(this, "mobile_agent_1", sharedPreferences.getString("server_ip", "?"), sharedPreferences.getString("server_port", "?"));

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase("APPEND_MESSAGE")) {
                try {
                    logTextView.append(intent.getExtras().getString("message") + "\n");
                } catch (Exception e) {
                    logger.log(Level.WARNING,
                            "Error in MyReceiver: " + e.getMessage());
                }
            }
        }
    }
}
