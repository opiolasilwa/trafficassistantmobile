package pl.edu.agh.traffic.assistant.mobile.agents;

/**
 * Created by Adiki on 14.04.14.
 */

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.preference.PreferenceManager;

import java.util.logging.Level;

import jade.android.AndroidHelper;
import jade.android.MicroRuntimeService;
import jade.android.MicroRuntimeServiceBinder;
import jade.android.RuntimeCallback;
import jade.core.MicroRuntime;
import jade.core.Profile;
import jade.util.Logger;
import jade.util.leap.Properties;
import pl.edu.agh.ta.common.map.AStarAlgorithm;
import pl.edu.agh.ta.common.map.StreetMap;
import pl.edu.agh.traffic.assistant.mobile.util.MobileLogger;
import pl.edu.agh.ta.common.jade.DriverAgent;

public class MobileAgentInstance {
    private Logger logger = Logger.getJADELogger(this.getClass().getName());

    private MicroRuntimeServiceBinder microRuntimeServiceBinder;
    private ServiceConnection serviceConnection;

    public MobileAgentInstance(final Activity mainActivity, final String name,
                               String host, String port) {
        final Properties profile = new Properties();
        profile.setProperty(Profile.MAIN_HOST, host);
        profile.setProperty(Profile.MAIN_PORT, port);
        profile.setProperty(Profile.MAIN, Boolean.FALSE.toString());
        profile.setProperty(Profile.JVM, Profile.ANDROID);

        if (AndroidHelper.isEmulator()) {
            // Emulator: this is needed to work with emulated devices
            profile.setProperty(Profile.LOCAL_HOST, AndroidHelper.LOOPBACK);
        } else {
            profile.setProperty(Profile.LOCAL_HOST,
                    AndroidHelper.getLocalIPAddress());
        }
        // Emulator: this is not really needed on a real device
        profile.setProperty(Profile.LOCAL_PORT, "2000");

        if (microRuntimeServiceBinder == null) {
            serviceConnection = new ServiceConnection() {
                public void onServiceConnected(ComponentName className,
                                               IBinder service) {
                    microRuntimeServiceBinder = (MicroRuntimeServiceBinder) service;
                    logger.log(Level.INFO,
                            "Gateway successfully bound to MicroRuntimeService");
                    startContainer(mainActivity, name, profile);
                }

                ;

                public void onServiceDisconnected(ComponentName className) {
                    microRuntimeServiceBinder = null;
                    logger.log(Level.INFO,
                            "Gateway unbound from MicroRuntimeService");
                }
            };
            logger.log(Level.INFO, "Binding Gateway to MicroRuntimeService...");
            mainActivity.bindService(
                    new Intent(mainActivity.getApplicationContext(),
                            MicroRuntimeService.class), serviceConnection,
                    Context.BIND_AUTO_CREATE
            );
        } else {
            logger.log(Level.INFO,
                    "MicroRumtimeGateway already bound to service");
            startContainer(mainActivity, name, profile);
        }
    }

    private void startContainer(final Activity mainActivity, final String name, Properties profile) {
        if (!MicroRuntime.isRunning()) {
            microRuntimeServiceBinder.startAgentContainer(profile,
                    new RuntimeCallback<Void>() {
                        @Override
                        public void onSuccess(Void thisIsNull) {
                            logger.log(Level.INFO,
                                    "Successful start of the container...");
                            doStartAgent(mainActivity, name);
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            logger.log(Level.SEVERE,
                                    "Failed to start the container...");
                        }
                    }
            );
        } else {

            doStartAgent(mainActivity, name);
        }
    }

    private void doStartAgent(Activity mainActivity, final String name) {
        Object agentArgs[] = new Object[]{
                new MobileLogger(mainActivity.getApplicationContext()),
                new StreetInfoRequestFactory(
                        PreferenceManager.getDefaultSharedPreferences(
                                mainActivity.getApplicationContext())
                ),
                AStarAlgorithm.getShortestRoute(StreetMap.fromResource("/map1.txt"), 0, 17),
                true};
        microRuntimeServiceBinder.startAgent(name,
                DriverAgent.class.getName(),
                agentArgs,
                new RuntimeCallback<Void>() {
                    @Override
                    public void onSuccess(Void thisIsNull) {
                        logger.log(Level.INFO, "Successfully start of the "
                                + DriverAgent.class.getName() + "...");
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        logger.log(Level.SEVERE, "Failed to start the "
                                + DriverAgent.class.getName() + "...");
                    }
                }
        );
    }
}
