package pl.edu.agh.traffic.assistant.mobile.util;

import android.content.Context;
import android.content.Intent;

import pl.edu.agh.ta.common.util.AbstractLogger;


/**
 * Created by lopiola on 13.05.14.
 */
public class MobileLogger extends AbstractLogger {

    private Context context;

    public MobileLogger(Context context) {
        this.context = context;
    }

    @Override
    public void log(String message) {
        Intent broadcast = new Intent();
        broadcast.setAction("APPEND_MESSAGE");
        broadcast.putExtra("message", message);
        context.sendBroadcast(broadcast);
    }
}
